# FACTORY PATTERN

Factory pattern is one of most used design patterns in Java. This type of design
pattern comes under creational pattern as this pattern provides one of the best
ways to create an object.
In Factory pattern, we create objects without exposing the creation logic to the
client and refer to newly created object using a common interface.


## Implementation
We are going to create a Shape interface and concrete classes implementing
the Shape interface. A factory class ShapeFactory is defined as a next step.
FactoryPatternDemo, our demo class, will use ShapeFactory to get a Shape object.
It will pass information (CIRCLE / RECTANGLE / SQUARE) to ShapeFactory to get
the type of object it needs.

### Steps :
- Step1 : Create an interface Shape.java
- Step2 : Create concrete classes implementing the same interface (Rectangle.java , Square.java , Circle.java)
- Step3 : Create a Factory to generate object of concrete class based on given information.
- Step4 : Use the Factory to get object of concrete class by passing an information such as
  type
- Step5 : Verify the output.


```
Inside Circle::draw() method.
Inside Rectangle::draw() method.
Inside Square::draw() method.

```



## Author🚀
- [Khaled Morad](https://gitlab.com/khaledmorad1)


## 🛠 Skills
Design patterns , OOP , Java.


## 🔗 About Me
If you have any questions or suggestions, feel free to reach out to us at :

[![GitLab](https://img.shields.io/badge/-Gitlab-orange?logo=gitlab&logoColor=white&style=for-the-badge)](https://gitlab.com/khaledmorad1)

[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/khaledmorad1)

[![gmail](https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white)](mailto:khaled.morad.br@gmail.com)



#### Good Luck and Thanks For Your Time

