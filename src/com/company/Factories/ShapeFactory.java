package com.company.Factories;

import com.company.Shapes.Circle;
import com.company.Shapes.Rectangle;
import com.company.Shapes.Shape;
import com.company.Shapes.Square;

public class ShapeFactory {
    //use getShape method to get object of type shape
    public Shape getShape(String shapeType) {
        if (shapeType == null) {
            return null;
        }
        //The equalsIgnoreCase() method compares two strings, ignoring lower case and upper case differences.

        if (shapeType.equalsIgnoreCase("CIRCLE")) {
            return new Circle();
        } else if (shapeType.equalsIgnoreCase("RECTANGLE")) {
            return new Rectangle();
        } else if (shapeType.equalsIgnoreCase("SQUARE")) {
            return new Square();
        }
        return null;
    }
}
